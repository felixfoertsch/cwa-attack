const { createEventQRCode } = require('cwa-event-qr-code')
const { createHash } = require('crypto')
const protobufjs = require('protobufjs')
const requestify = require('requestify')
const request = require('request')
const forge = require('node-forge')
const JSZip = require('jszip')

function getQRCodePayload(string) {
	const QRCodePayload = getProtoClass('trace_location.proto', 'QRCodePayload')
	const qrCodePayloadBytes = Buffer.from(string, 'base64url')
	const message = QRCodePayload.decode(qrCodePayloadBytes)

	return QRCodePayload.toObject(message)
}

function toByteArray(string) {
	var bytes = []
	for (var i = 0; i < string.length; i++) {
		var char = string.charCodeAt(i)
		bytes.push(char >>> 8)
		bytes.push(char & 0xFF)
	}
	return bytes
}

function toHexString(byteArray) {
	return Array.from(byteArray, function (byte) {
		return ('0' + (byte & 0xFF).toString(16)).slice(-2)
	}).join('')
}

function getProtoClass(protoFileName, className) {
	const root = protobufjs.loadSync(protoFileName)
	const jsonDescriptor = protobufjs.Root.fromJSON(root.toJSON())

	return jsonDescriptor.lookupType(className)
}

function getMac(macKey, iv, encryptedCheckInRecord) {
	const mac = createHash('sha256')
	mac.update(macKey)
	mac.update(iv)
	mac.update(encryptedCheckInRecord)

	return mac.digest('base64')
}

function isIdenitcalTraceLocationIdHash(local, remote) {
	// If TRUE there is an infection at the location and we are also able to decrypt the content
	return local == remote
}

const eventQRCode0 = createEventQRCode({
	locationData: {
		description: 'Wilde Beschreibung 0',
		address: 'Wilde Adresse 0'
	},
	vendorData: {
		type: 1,
		defaultCheckInLengthInMinutes: 30
	}
})

const eventQRCode1 = createEventQRCode({
	locationData: {
		description: 'Wilde Beschreibung 1',
		address: 'Wilde Adresse 1'
	},
	vendorData: {
		type: 1,
		defaultCheckInLengthInMinutes: 30
	}
})

var qrcodes = {
	'0': 'https://e.coronawarn.app?v=1#CAESDQgBEgJNeRoFUGxhY2UadggBEmCDAszMTXne1DAA5_YxmhRdd_NZN2VKl9L32Jl9-ZybE4b2eNIrhFOKYU4XAOHq3RPLDxdHTW6ANiO24rCOO4rj06HzcVZy3pel58-L1KSPG-_PneL2BoyZQRz3qlu2hoAaEBMWSEe1lrN20-gitEkvDzIiBggBEAMYeA',
	'1': await eventQRCode0.toURL(),
	'2': await eventQRCode1.toURL()
}

// http://localhost:8003/cwa/version/v2/twp/country/DE/hour/456358
requestify.get('http://localhost:8003/cwa/version/v2/twp/country/DE/hour').then((response) => {
	const latest = response.getBody().latest
	const latest_package = 'http://localhost:8003/cwa/version/v2/twp/country/DE/hour/' + latest

	const object = request(
		{ method: "GET", url: latest_package, encoding: null },
		function (error, response, body) {
			JSZip.loadAsync(body).then(
				function (zip) {
					return zip.file("export.bin").async("array")
				}).then(
					function (byteArray) {
						const TraceWarningPackage = getProtoClass('trace_warning_package.proto', 'TraceWarningPackage')
						const traceWarningPackage = TraceWarningPackage.decode(byteArray)

						Object.entries(qrcodes).forEach((entry) => {
							const urlPayload = entry[1].split('?v=1#')[1]
							const qrCodePayload = getQRCodePayload(urlPayload)

							const cwaDomain = toByteArray('CWA-GUID')
							const QRCodePayload = getProtoClass('trace_location.proto', 'QRCodePayload')
							const payloadUint8array = QRCodePayload.encode(qrCodePayload).finish()
							const payloadByteArray = Array.from(payloadUint8array)
							const totalByteSequence = Buffer.from(cwaDomain.concat(payloadByteArray))
							const traceLocationIdHash = createHash('sha256').update(Buffer.from(totalByteSequence)).digest('hex')

							// TODO: 10-Minuten-Intervalle verwenden
							const startInterval = Date.now()
							const endInterval = Date.now()

							const CheckIn = getProtoClass('check_in.proto', 'CheckIn')
							var checkIn = CheckIn.create({
								locationId: traceLocationIdHash,
								startIntervalNumber: startInterval,
								endIntervalNumber: endInterval,
								transmissionRiskLevel: 1
							})

							// TODO: Checkin komplett fertig bauen (inkl Zeit-Intervalle a 10 Minuten)

							// TODO: Checkin verschlüsseln, TraceLocationIdHash generieren, das ist notwendig,
							// damit wir LocationIdHashes aus den empfangenen Warnungen abgleichen können
							// Siehe: CheckInWarningMatcher.kt Z. 126 -> deriveTraceWarnings()

							// Entschlüsselung
							traceWarningPackage['checkInProtectedReports'].forEach((report) => {
								const locationIdHash = toHexString(report['locationIdHash'])

								if (!isIdenitcalTraceLocationIdHash(traceLocationIdHash, locationIdHash)) { return }

								const CWA_MAC_KEY = Array.from(Buffer.from('4357412d4d41432d4b4559', 'hex'))
								const traceLocationId = Array.from(Buffer.from('locationId', 'utf8'))

								const cwaMacKey = createHash('sha256').update(CWA_MAC_KEY + traceLocationId).digest('hex')
								const encryptedCheckInRecord = report['encryptedCheckInRecord']
								const iv = report['iv']

								const macCheckInProtectedReport = report['mac']
								let macGenerated = forge.hmac.create()
								macGenerated.start('sha256', cwaMacKey)
								macGenerated.update(Array.from(Buffer.from(iv + encryptedCheckInRecord)))
								let macGeneratedDigest = macGenerated.digest().toHex()
								console.log(macGeneratedDigest)

								const CWA_ENCRYPTION_KEY = Array.from(Buffer.from('4357412d454e4352595054494f4e2d4b4559', 'hex'))
								const cwaEncryptionKey = createHash('sha256').update(CWA_ENCRYPTION_KEY + traceLocationId).digest('hex')

								var decipher = forge.cipher.createDecipher('AES-CBC', Array.from(Buffer.from(cwaEncryptionKey, 'hex')))
								decipher.start({ iv: iv })
								decipher.update(forge.util.createBuffer(encryptedCheckInRecord))
								decipher.finish()

								const decryptedCheckinBinary = decipher.output.data
								const CheckInRecord = getProtoClass('trace_warning_package.proto', 'CheckInRecord')
							})
						})
					})
		})
})